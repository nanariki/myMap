import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        MyMap<String, Integer> myMap = new MyMap<>(1, 13);
        HashMap<String, Integer> testMap = new HashMap<>();
        for (int i = 0; i < 50; i++) {
            String k = UUID.randomUUID().toString();
            int v = random.nextInt();
            testMap.put(k, v);
            myMap.put(k, v);
            if (!myMap.containsKey(k) || !myMap.get(k).equals(v)) {
                throw new RuntimeException("Missing entry");
            }
            System.out.println(myMap.size());

        }

        if(testMap.size() != myMap.size()) {
            throw new RuntimeException("Different Size");
        }

        for (Map.Entry<String, Integer> e: testMap.entrySet() ) {
            if (!myMap.containsKey(e.getKey()) || !myMap.get(e.getKey()).equals(e.getValue())) {

                throw new RuntimeException("Missing entry");

            }
            myMap.remove(e.getKey());
        }


        if(!myMap.isEmpty()) {
            throw new RuntimeException("Leftover values");
        }
    }
}
