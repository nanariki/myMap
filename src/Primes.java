/**
 * Created by youngz on 26/04/16.
 */
import java.util.BitSet;

public class Primes {

    private final int size;
    private final BitSet bits;

    public Primes(int size) {
        this.size = size;
        this.bits = new BitSet(this.size);

        for (int i = 2; i < this.size; i++) {
            bits.set(i);
        }

        int i = 2;
        while (i * i < this.size) {
            if (bits.get(i)) {
                int k = 2 * i;
                while (k <= this.size) {
                    bits.clear(k);
                    k += i;
                }
            }
            i++;
        }
    }
    public int getSize() {
        return size;
    }

    /**
     * Returns the smallest prime greater than val
     */
    public int getNextPrime(int val) {
        for(int i = val; i < size; i++) {
            if(this.bits.get(i)) {
                return i;
            }
        }

        return this.size;
    }

    public int getLastPrime(int val){
        for (int i = val-1; i > 0; i --){
            if (this.bits.get(i)){
                return i;
            }
        }
        return this.size;
    }
}
