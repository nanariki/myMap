import sun.security.x509.AVA;

import java.util.*;

/**
 * Created by youngz on 20/04/16.
 */
public class MyMap<K, V> implements Map<K, V> {

    @SuppressWarnings("unchecked")
    private MyEntry<K, V>[] set;
    private double load;
    private int numEntries;
    private int a;
    private int b;
    private int q;
    private Primes prime;
    private final static boolean INSERT = true;
    private final static boolean FIND = false;
    private MyEntry<K,V> AVAILABLE = new MyEntry();

    public MyMap(double load, int capacity) {

        this.prime = new Primes(100_000_000);
        this.set = (MyEntry<K, V>[]) new MyEntry[this.prime.getNextPrime((int)Math.round(capacity/load))];
        this.load = load;
        this.numEntries = 0;
        this.calcVar();
    }

    @Override
    public int size() {
        return this.numEntries;
    }

    @Override
    public boolean isEmpty() {
        return this.set.length == 0;
    }

    @Override
    public boolean containsKey(Object o) {
        int index = this.getPosition(o, FIND);
        if(this.set[index] == null){
            return false;
        }else if (this.set[index].getKey().equals(o)){
            return true;
        }
        return false;
    }

    @Override
    public boolean containsValue(Object o) {
        for (MyEntry<K, V> entry : this.set) {
            if (entry != null && entry.getValue().equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public V get(Object o) {

        int index = this.getPosition(o, FIND);
        if (this.set[index] == null){
            return null;
        }else{
            return this.set[index].getValue();
        }
    }

    private int getPosition(Object o, boolean insert){
        int starIndex = this.compressionHash(o.hashCode());
        int sHash = this.secondHash(o.hashCode());
        int curIndex = 0;
        int j = 0;

        while (this.set[starIndex] != null){
            if (this.set[starIndex].getKey().equals(o) && !insert){
                break;
            }
            if (this.set[starIndex].equals(AVAILABLE) && insert) {
                break;
            }
            curIndex = ((starIndex + j*sHash) % this.set.length);
            j ++;
            if (curIndex < 0 || j > this.set.length){
                break;
            }
        }
        return curIndex;
    }

    private void insert(K key, V value) {
        this.set[this.getPosition(key, INSERT)] = new MyEntry<>(key, value);
    }

    private void calcVar() {
        this.a = this.prime.getNextPrime(this.set.length + 1);
        this.b = this.prime.getNextPrime(this.set.length + 1);
        this.q = this.prime.getLastPrime(this.set.length);
    }

    private void resize(int size){
        MyEntry<K, V>[] oldSet = this.set;
        this.set = (MyEntry<K, V>[]) new MyEntry[this.prime.getNextPrime(size)];
        this.calcVar();
        for (int i=0; i <oldSet.length; i ++) {
            if (oldSet[i] != null) {
                this.insert(oldSet[i].getKey(), oldSet[i].getValue());
            }
        }
    }

    @Override
    public V put(K k, V v) {
        // if the array is too full, double size and rehash entries into new array
        if (((this.numEntries + 1) / this.load) > this.set.length) {
            this.resize(this.set.length * 2);
        }

        this.insert(k, v);
        this.numEntries++;
        //TODO return the previous element
        return v;
    }

    @Override
    public V remove(Object o) {
        int index = this.getPosition(o, FIND);
        if (this.set[index] == null){
            return null;
        }else{
             V value = this.set[index].getValue();
            this.set[index] = AVAILABLE;
            return value;
        }
    }

    private int compressionHash(int value) {
        return Math.abs(this.a * value + this.b) % this.set.length;

    }

    private int secondHash(int value) {
        return this.q - Math.abs(value) % this.q;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Entry<? extends K, ? extends V> entry : map.entrySet()) {
            this.put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        Arrays.fill(this.set, null);
    }

    @Override
    public Set<K> keySet() {
        return null;
    }

    @Override
    public Collection<V> values() {
        return null;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return null;
    }

    private class MyEntry<K, V> implements Entry<K, V> {

        private K k;
        private V v;

        public MyEntry(){}

        public MyEntry(K k, V v) {
            this.k = k;
            this.v = v;
        }

        @Override
        public K getKey() {
            return k;
        }

        @Override
        public V getValue() {
            return v;
        }

        @Override
        public V setValue(V v) {
            this.v = v;
            return v;
        }
    }

}
